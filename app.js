;(function ( $, window, document, undefined ) {
	var app = {
		domainList :   ['sfbay.craigslist.org',
						'losangeles.craigslist.org',
						'newyork.craigslist.org',
						'seattle.craigslist.org',
						'chicago.craigslist.org',
						'orangecounty.craigslist.org',
						'portland.craigslist.org',
						'washingtondc.craigslist.org',
						'sandiego.craigslist.org',
						'boston.craigslist.org',
						'denver.craigslist.org',
						'sacramento.craigslist.org',
						'dallas.craigslist.org',
						'minneapolis.craigslist.org',
						'phoenix.craigslist.org',
						'geo.craigslist.org',
						'atlanta.craigslist.org',
						'inlandempire.craigslist.org',
						'austin.craigslist.org',
						'miami.craigslist.org',
						'philadelphia.craigslist.org',
						'houston.craigslist.org',
						'newjersey.craigslist.org',
						'detroit.craigslist.org',
						'tampa.craigslist.org',
						'orlando.craigslist.org',
						'raleigh.craigslist.org',
						'lasvegas.craigslist.org',
						'cnj.craigslist.org',
						'baltimore.craigslist.org',
						'charlotte.craigslist.org',
						'milwaukee.craigslist.org',
						'nashville.craigslist.org',
						'kansascity.craigslist.org',
						'columbus.craigslist.org',
						'nh.craigslist.org',
						'stlouis.craigslist.org',
						'ventura.craigslist.org',
						'honolulu.craigslist.org',
						'sanantonio.craigslist.org',
						'cincinnati.craigslist.org',
						'richmond.craigslist.org',
						'hudsonvalley.craigslist.org',
						'indianapolis.craigslist.org',
						'hartford.craigslist.org',
						'providence.craigslist.org',
						'pittsburgh.craigslist.org',
						'maine.craigslist.org',
						'madison.craigslist.org',
						'boulder.craigslist.org',
						'longisland.craigslist.org',
						'cleveland.craigslist.org',
						'tucson.craigslist.org',
						'southjersey.craigslist.org',
						'grandrapids.craigslist.org',
						'forums.craigslist.org',
						'bellingham.craigslist.org',
						'cosprings.craigslist.org',
						'worcester.craigslist.org',
						'spokane.craigslist.org',
						'santabarbara.craigslist.org',
						'newhaven.craigslist.org',
						'greenville.craigslist.org',
						'albany.craigslist.org',
						'eugene.craigslist.org',
						'boise.craigslist.org',
						'reno.craigslist.org',
						'fortcollins.craigslist.org',
						'asheville.craigslist.org',
						'annarbor.craigslist.org',
						'vermont.craigslist.org',
						'sarasota.craigslist.org',
						'fortmyers.craigslist.org',
						'westernmass.craigslist.org',
						'knoxville.craigslist.org',
						'images.craigslist.org',
						'oklahomacity.craigslist.org',
						'louisville.craigslist.org',
						'jacksonville.craigslist.org',
						'fresno.craigslist.org',
						'stockton.craigslist.org',
						'slo.craigslist.org',
						'modesto.craigslist.org',
						'greensboro.craigslist.org',
						'rochester.craigslist.org',
						'palmsprings.craigslist.org',
						'buffalo.craigslist.org',
						'norfolk.craigslist.org',
						'bakersfield.craigslist.org',
						'jerseyshore.craigslist.org',
						'dayton.craigslist.org',
						'charleston.craigslist.org',
						'akroncanton.craigslist.org',
						'tulsa.craigslist.org',
						'monterey.craigslist.org',
						'albuquerque.craigslist.org',
						'syracuse.craigslist.org',
						'lancaster.craigslist.org',
						'neworleans.craigslist.org',
						'omaha.craigslist.org',
						'newlondon.craigslist.org',
						'allentown.craigslist.org',
						'salem.craigslist.org',
						'springfield.craigslist.org',
						'desmoines.craigslist.org',
						'delaware.craigslist.org',
						'chico.craigslist.org',
						'columbiamo.craigslist.org',
						'columbia.craigslist.org',
						'bend.craigslist.org',
						'winstonsalem.craigslist.org',
						'harrisburg.craigslist.org',
						'daytona.craigslist.org',
						'chattanooga.craigslist.org',
						'secure.craigslist.org',
						'gainesville.craigslist.org',
						'appleton.craigslist.org',
						'southbend.craigslist.org',
						'redding.craigslist.org',
						'ocala.craigslist.org',
						'lakeland.craigslist.org',
						'fredericksburg.craigslist.org',
						'corvallis.craigslist.org',
						'bham.craigslist.org',
						'anchorage.craigslist.org',
						'spacecoast.craigslist.org',
						'saltlakecity.craigslist.org',
						'rockford.craigslist.org',
						'fayar.craigslist.org',
						'lexington.craigslist.org',
						'lansing.craigslist.org',
						'huntsville.craigslist.org',
						'charlottesville.craigslist.org',
						'toledo.craigslist.org',
						'memphis.craigslist.org',
						'medford.craigslist.org',
						'goldcountry.craigslist.org',
						'athensga.craigslist.org',
						'annapolis.craigslist.org',
						'york.craigslist.org',
						'stcloud.craigslist.org',
						'greenbay.craigslist.org',
						'eauclaire.craigslist.org',
						'kalamazoo.craigslist.org',
						'eastnc.craigslist.org',
						'chambana.craigslist.org',
						'tallahassee.craigslist.org',
						'lincoln.craigslist.org',
						'fortwayne.craigslist.org',
						'easttexas.craigslist.org',
						'bozeman.craigslist.org',
						'wichita.craigslist.org',
						'tricities.craigslist.org',
						'treasure.craigslist.org',
						'southcoast.craigslist.org',
						'savannah.craigslist.org',
						'nwct.craigslist.org',
						'missoula.craigslist.org',
						'fayetteville.craigslist.org',
						'yakima.craigslist.org',
						'rmn.craigslist.org',
						'reading.craigslist.org',
						'pensacola.craigslist.org',
						'hickory.craigslist.org',
						'wilmington.craigslist.org',
						'merced.craigslist.org',
						'littlerock.craigslist.org',
						'kpr.craigslist.org',
						'killeen.craigslist.org',
						'frederick.craigslist.org',
						'easternshore.craigslist.org',
						'duluth.craigslist.org',
						'capecod.craigslist.org',
						'skagit.craigslist.org',
						'racine.craigslist.org',
						'flagstaff.craigslist.org',
						'collegestation.craigslist.org',
						'batonrouge.craigslist.org',
						'rockies.craigslist.org',
						'roanoke.craigslist.org',
						'myrtlebeach.craigslist.org',
						'mobile.craigslist.org',
						'iowacity.craigslist.org',
						'centralmich.craigslist.org',
						'cedarrapids.craigslist.org',
						'bloomington.craigslist.org',
						'wenatchee.craigslist.org',
						'wausau.craigslist.org',
						'smd.craigslist.org',
						'nwga.craigslist.org',
						'nmi.craigslist.org',
						'mcallen.craigslist.org',
						'flint.craigslist.org',
						'elpaso.craigslist.org',
						'binghamton.craigslist.org',
						'youngstown.craigslist.org',
						'scranton.craigslist.org',
						'quadcities.craigslist.org',
						'prescott.craigslist.org',
						'muskegon.craigslist.org',
						'mankato.craigslist.org',
						'macon.craigslist.org',
						'fargo.craigslist.org',
						'altoona.craigslist.org',
						'waco.craigslist.org',
						'tippecanoe.craigslist.org',
						'santamaria.craigslist.org',
						'pullman.craigslist.org',
						'lynchburg.craigslist.org',
						'lacrosse.craigslist.org',
						'joplin.craigslist.org',
						'ithaca.craigslist.org',
						'humboldt.craigslist.org',
						'dubuque.craigslist.org',
						'augusta.craigslist.org',
						'westslope.craigslist.org',
						'swmi.craigslist.org',
						'siouxcity.craigslist.org',
						'sanmarcos.craigslist.org',
						'oregoncoast.craigslist.org',
						'olympic.craigslist.org',
						'limaohio.craigslist.org',
						'hattiesburg.craigslist.org',
						'yuma.craigslist.org',
						'waterloo.craigslist.org',
						'staugustine.craigslist.org',
						'siouxfalls.craigslist.org',
						'santafe.craigslist.org',
						'poconos.craigslist.org',
						'panamacity.craigslist.org',
						'muncie.craigslist.org',
						'holland.craigslist.org',
						'fortsmith.craigslist.org',
						'erie.craigslist.org',
						'eastidaho.craigslist.org',
						'carbondale.craigslist.org',
						'ames.craigslist.org',
						'topeka.craigslist.org',
						'springfieldil.craigslist.org',
						'sheboygan.craigslist.org',
						'semo.craigslist.org',
						'pueblo.craigslist.org',
						'peoria.craigslist.org',
						'pennstate.craigslist.org',
						'northernwi.craigslist.org',
						'lawrence.craigslist.org',
						'janesville.craigslist.org',
						'evansville.craigslist.org',
						'dothan.craigslist.org',
						'clarksville.craigslist.org',
						'bn.craigslist.org',
						'blacksburg.craigslist.org',
						'stgeorge.craigslist.org',
						'saginaw.craigslist.org',
						'loz.craigslist.org',
						'jackson.craigslist.org',
						'gulfport.craigslist.org',
						'auburn.craigslist.org',
						'winchester.craigslist.org',
						'valdosta.craigslist.org',
						'utica.craigslist.org',
						'okaloosa.craigslist.org',
						'northmiss.craigslist.org',
						'lubbock.craigslist.org',
						'lafayette.craigslist.org',
						'ksu.craigslist.org',
						'keys.craigslist.org',
						'kalispell.craigslist.org',
						'harrisonburg.craigslist.org',
						'glensfalls.craigslist.org',
						'corpuschristi.craigslist.org',
						'chautauqua.craigslist.org',
						'wyoming.craigslist.org',
						'watertown.craigslist.org',
						'texoma.craigslist.org',
						'roseburg.craigslist.org',
						'provo.craigslist.org',
						'morgantown.craigslist.org',
						'mohave.craigslist.org',
						'klamath.craigslist.org',
						'huntington.craigslist.org',
						'elmira.craigslist.org',
						'billings.craigslist.org',
						'up.craigslist.org',
						'shreveport.craigslist.org',
						'lascruces.craigslist.org',
						'jacksontn.craigslist.org',
						'dublin.craigslist.org',
						'dubai.craigslist.org',
						'brownsville.craigslist.org',
						'fairbanks.craigslist.org',
						'cairo.craigslist.org',
						'amsterdam.craigslist.org',
						'dublin.craigslist.org',
						'amarillo.craigslist.org',
						'plattsburgh.craigslist.org',
						'costarica.craigslist.org',
						'puertorico.craigslist.org',
						'malaysia.craigslist.org',
						'auckland.craigslist.org'],
		domainSuffix : '/search/jjj?',
		searchProps : {
			query : 'wordpress',
			sort : 'date',
			srchType : 'T',	
			is_telecommuting: '1',
		},
		usedIDs : [],
		postCount : 0,
		form : $('.form'),
		searchField : $('.form-text'),
		posts : $('.posts'),
		postsloading : $('.postsloading'),
		_init : function(){
			let querySuffix = $.param(app.searchProps);
			app.domainList.forEach(function(domain, index){	
				let querySuffix = $.param(app.searchProps);
				let url = 'https://' + domain + app.domainSuffix + querySuffix;
				console.log(url);

				app.domainList.forEach(function(domain, index){	
					let url = 'http://' + domain + app.domainSuffix + querySuffix;
					$.ajax({
						url: 'load.php',
						type: 'POST',
						data: {
							url: url,
						}
					})
					.done(function(response) {
						$.each( $('li.result-row', $(response) ), app._parseCLPost.bind(null, domain) );
					})
					.fail(function() {
						console.log("error");
					});
				});
			});			
			$(document).ajaxStop(function() {
				console.log('ajaxstop');
				app.postsloading.slideUp().fadeOut();
			});
			
		},
		_loadXMLDoc : function(theURL, domain){
            var xmlhttp = new XMLHttpRequest();

	        
	        xmlhttp.onreadystatechange = function(){
	            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){	 
	            	// for every post that appears on a domains' search page - parse the post for title, link, domain, timestamp and description        	
	            	$.each($('li.result-row', $(xmlhttp.response)), app._parseCLPost.bind(null, xmlhttp, domain));
	            }
	        }
	        
	        xmlhttp.open("GET", theURL, true);
			xmlhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
	        xmlhttp.send();
	    },
		_parseCLPost : function(xmlhttp, domain, index, result){
			if(app.postCount < 200){
	    		// let pageLink = $('a.hdrlnk', $(xmlhttp.response)).attr('href');
	    		let pageLink = $('a.hdrlnk',$(result)).attr('href');
	    		let pageID = $('a.hdrlnk', $(result)).attr('data-id');
	    		let postTitle = $('a.hdrlnk', $(result)).text();
	    		let postTime = new Date($('time', $(result)).attr('datetime'));	
	    		let timeAgo = new Date();
	    		timeAgo = timeAgo.setDate(timeAgo.getDate() - 7);
	    		// console.log( postTime, Date.now() - timeAgo );
	    		if( pageLink != undefined && $.inArray(pageID, app.usedIDs) == -1 && Date.parse(postTime) >= Date.now() - timeAgo ){
	    			// if pageLink starts with '//'
	    			if( pageLink.match(/(\/\/)\S+/) == null ){
	    				pageLink = 'https://' + domain + pageLink;
	    			}
	    			// if pageLink starts with 'http://'
	    			else if( pageLink.match(/(https:\/\/)/) ){
	    				pageLink = pageLink;
	    			}
	    			else{
	    				pageLink = 'https:' + pageLink;
	    			}
	    			let postDomain = pageLink.match(/https:\/\/(.*?)\//)[1];
					// console.log(pageLink, postTime, postTitle, postDomain);
	    			$.ajax({
	    				url: pageLink
	    			}).success(app._buildPostString.bind(null, pageLink, postTime, postTitle, postDomain));
	    		}
	    		app.usedIDs.push(pageID);
	    	}
	    	else{
	    		app.posts.append('<div>Sorry please refresh your page or try a different search. We\'ve got too many results.</div>');
	    	}
		},
		_buildPostString : function(pageLink, postTime, postTitle, postDomain){
			let unixTime = new Date(postTime).getTime()/1000;
			postString =   `<div class="post" data-unixtime="${unixTime}">
								<a target="_blank" class="post-link" href="${pageLink}">
									${postTitle}
								</a>
								<span class="post-domain">From ${postDomain}</span>
								<span class="post-time">Posted on ${$.format.date(postTime, 'MMM D yyyy h:mm a')}</span>
							</div>`;

			app.posts.append(postString);
			$('.post:last-of-type').hide().slideDown();	
			app.postCount++;			
		},
	}
	app._init();
})( jQuery, window, document );