<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Scrape Craigslist</title>
	<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="jquery-dateFormat.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Lato:400,300,600' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="app.css"></link>
</head>
<body>
	<div class="container">
		<h1 class="title">CL Jobs Search</h1>
		<h3 class="subtitle">Searches 297 Craigslist subdomains for your keywords in posting titles</h3>	
		
		<div class="posts">
			
		</div>
		<img class="postsloading" src="loading.gif">
		<footer class="footer">
			Not a &copy;<?php echo Date('Y') ?> of anything, ever.
		</footer>
	</div>
	<script type="text/javascript" src="app.js"></script>
</body>
</html>